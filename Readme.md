# scriptED v0.5-Alpha

![image](screenshots/scriptED.png)

## Freatures
* Film screenplay/script writing.
* Easy shortcuts for formatting.
* Built-in AI assistant. Using local models.
* Export : Fountain or plain text.
* No data goes to the cloud. All stored locally.
* Send full scripts as chunks to the AI for chat.
* AI long term memory, with ability to clear it.

# Requirements.
* llama.cpp
* libcurl
* jsoncpp
* gpgme
* gtkmm 4
* cmake

# Install
## llama.cpp and scriptED
```
cd ~/
mkdir temp
cd temp
git clone https://gitlab.com/leestripp/llamacpp.git
mkdir ./llamacpp/build
cd llamacpp/build
cmake -DCMAKE_INSTALL_PREFIX=~/.local/llamacpp -DCMAKE_BUILD_TYPE=Release ..
make
make install

cd ~/temp

git clone https://gitlab.com/leestripp/scripted.git
mkdir ./scripted/build
cd scripted/build
cmake ..
make
make install
```
scriptED is installed into your home directory. "~/.local/bin"

Run:
```
scripted_start
```
OR

Run it from your Linux desktop menu system.

