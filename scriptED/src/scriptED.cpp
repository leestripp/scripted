#include <iostream>

#include "scriptED.h"
// Menus
#include "menus/mainmenu.h"

using namespace std;

scriptED::scriptED( Glib::RefPtr<Gtk::Application> a ) :
	m_VBox(Gtk::Orientation::VERTICAL),
	m_HPaned(Gtk::Orientation::HORIZONTAL),
	m_msg_dialog( *this, "Message" ),
	m_page_box(Gtk::Orientation::HORIZONTAL)
{
	app = a;
	
	// set main window
	m_lschatai.set_MainWindow( this );
	
	// Messages
	m_msg_dialog.signal_response().connect( sigc::hide( sigc::mem_fun( m_msg_dialog, &Gtk::Widget::hide ) ) );
	
	// Set window settings.
	set_title ( "scriptED" );
	set_default_size( 1920, 1080 );
	
	// UI Builder
	auto m_refBuilder = Gtk::Builder::create();
	
	// Main Menu
	m_refBuilder->add_from_string( mainmenu_info );
	auto gmenu = m_refBuilder->get_object<Gio::Menu>( "menubar" );
	auto pMenuBar = Gtk::make_managed<Gtk::PopoverMenuBar>( gmenu );
	m_VBox.append( *pMenuBar );
	
	// Actions
	auto m_refActionGroup = Gio::SimpleActionGroup::create();
	// File
	m_refActionGroup->add_action( "open_project", sigc::mem_fun( *this, &scriptED::on_action_openproject ) );
	m_refActionGroup->add_action( "save", sigc::mem_fun( *this, &scriptED::on_action_save ) );
	m_refActionGroup->add_action( "export_plaintext", sigc::mem_fun( *this, &scriptED::on_action_export_plaintext ) );
	m_refActionGroup->add_action( "export_fountain", sigc::mem_fun( *this, &scriptED::on_action_export_fountain ) );
	// View
	m_refActionGroup->add_action( "view_zoomin", sigc::mem_fun( *this, &scriptED::on_action_view_zoomin) );
	m_refActionGroup->add_action( "view_zoomout", sigc::mem_fun( *this, &scriptED::on_action_view_zoomout) );
	// Format
	m_refActionGroup->add_action( "format_scene", sigc::mem_fun( *this, &scriptED::on_action_format_scene) );
	m_refActionGroup->add_action( "format_action", sigc::mem_fun( *this, &scriptED::on_action_format_action ) );
	m_refActionGroup->add_action( "format_character", sigc::mem_fun( *this, &scriptED::on_action_format_character ) );
	m_refActionGroup->add_action( "format_parenthetical", sigc::mem_fun( *this, &scriptED::on_action_format_parenthetical ) );
	m_refActionGroup->add_action( "format_dialogue", sigc::mem_fun( *this, &scriptED::on_action_format_dialogue ) );
	m_refActionGroup->add_action( "format_transition", sigc::mem_fun( *this, &scriptED::on_action_format_transition ) );
	m_refActionGroup->add_action( "format_shot", sigc::mem_fun( *this, &scriptED::on_action_format_shot ) );
	m_refActionGroup->add_action( "format_notes", sigc::mem_fun( *this, &scriptED::on_action_format_notes ) );
	m_refActionGroup->add_action( "format_plaintext", sigc::mem_fun( *this, &scriptED::on_action_format_plaintext ) );
	m_refActionGroup->add_action( "format_act", sigc::mem_fun( *this, &scriptED::on_action_format_act ) );
	// Ai
	m_refActionGroup->add_action( "lsllamacpp_ingest", sigc::mem_fun( *this, &scriptED::on_action_ingest_current) );
	m_refActionGroup->add_action( "lsllamacpp_rewrite", sigc::mem_fun( *this, &scriptED::on_action_rewrite_selection) );
	m_refActionGroup->add_action( "lsllamacpp_cleardb", sigc::mem_fun( *this, &scriptED::on_action_clear_database) );
	// Tools
	m_refActionGroup->add_action( "tools_splitscript", sigc::mem_fun( *this, &scriptED::on_action_split_scenes) );
	m_refActionGroup->add_action( "tools_splitacts", sigc::mem_fun( *this, &scriptED::on_action_split_acts) );
	m_refActionGroup->add_action( "tools_locations", sigc::mem_fun( *this, &scriptED::on_action_locations) );
	m_refActionGroup->add_action( "tools_characters", sigc::mem_fun( *this, &scriptED::on_action_characters) );
	// Add the group
	insert_action_group( "mainmenu", m_refActionGroup );
	
	// shortcuts
	// File
	app->set_accel_for_action("mainmenu.open_project", "<Primary>o");
	app->set_accel_for_action("mainmenu.save", "<Primary>s");
	app->set_accel_for_action("mainmenu.export_plaintext", "<Primary>e");
	app->set_accel_for_action("mainmenu.export_fountain", "<Primary>f");
	// View
	app->set_accel_for_action("mainmenu.view_zoomin", "<Primary>j");
	app->set_accel_for_action("mainmenu.view_zoomout", "<Primary>k");
	// Format
	app->set_accel_for_action("mainmenu.format_scene", "<Primary>1");
	app->set_accel_for_action("mainmenu.format_action", "<Primary>2");
	app->set_accel_for_action("mainmenu.format_character", "<Primary>3");
	app->set_accel_for_action("mainmenu.format_parenthetical", "<Primary>4");
	app->set_accel_for_action("mainmenu.format_dialogue", "<Primary>5");
	app->set_accel_for_action("mainmenu.format_transition", "<Primary>6");
	app->set_accel_for_action("mainmenu.format_shot", "<Primary>7");
	app->set_accel_for_action("mainmenu.format_act", "<Primary>8");
	// AI
	app->set_accel_for_action("mainmenu.lsllamacpp_rewrite", "<Primary>r");
	
	// Scroll window in a box to limit width.
	// Page size
	float width = 800.0 * m_textview.get_zoomfactor();
	m_page_box.set_size_request( int(width), -1 );
	m_page_box.set_vexpand( true );
	m_page_box.set_hexpand( false );
	m_textview_ScrolledWindow.set_child( m_textview );
	m_page_box.append( m_textview_ScrolledWindow );
	m_textview_ScrolledWindow.set_size_request( int(width), -1 );
	m_textview_ScrolledWindow.set_hexpand( false );
	
	// TreeView - Textview splitter
	m_HPaned.set_margin( 10 );
	m_HPaned.set_position( 320 );
	m_HPaned.set_start_child( m_treeview );
	m_HPaned.set_end_child( m_page_box );
	
	// lsChatAI splitter
	m_HPanedAI.set_margin( 10 );
	m_HPanedAI.set_position( 1320 );
	m_HPanedAI.set_start_child( m_HPaned );
	m_HPanedAI.set_end_child( m_lschatai );
	
	m_VBox.append( m_HPanedAI );
	set_child( m_VBox );
	
	// links
	m_treeview.set_TextView( &m_textview );
	
}

scriptED::~scriptED()
{
}

// Overrides

void scriptED::on_realize()
{
	// debug
	// cout << "scriptED::on_realise() *** called..." << endl;
	
	Gtk::Window::on_realize();
	
	// Get display size
	auto display = get_display();
	auto surface = get_surface();
	if( display && surface )
	{
		auto monitor = display->get_monitor_at_surface( surface );
		Gdk::Rectangle geometry;
		monitor->get_geometry( geometry );
		
		set_default_size( geometry.get_width(), geometry.get_height() );
		
		m_HPaned.set_position( geometry.get_width() * 0.2 );
		m_HPanedAI.set_position( geometry.get_width() * 0.7 );
	} else
	{
		cerr << "ERROR: get display or surface failed..." << endl;
	}
}


// Open project

void scriptED::on_action_openproject()
{
	// debug
	// cout << "scriptED::on_action_openproject - clicked..." << endl;
	
	auto dialog = new Gtk::FileChooserDialog( "Choose project folder", Gtk::FileChooser::Action::SELECT_FOLDER );
	
	dialog->set_transient_for(*this);
	dialog->set_modal(true);
	dialog->signal_response().connect( sigc::bind( sigc::mem_fun( *this, &scriptED::on_folder_dialog_response), dialog ) );
	
	//Add response buttons to the dialog:
	dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
	dialog->add_button("Select", Gtk::ResponseType::OK);

	//Show the dialog and wait for a user response:
	dialog->show();
}

void scriptED::on_folder_dialog_response( int response_id, Gtk::FileChooserDialog* dialog )
{
	//Handle the response:
	switch( response_id )
	{
		case Gtk::ResponseType::OK:
		{
			// debug
			// cout << "Folder selected : " << dialog->get_file()->get_path() << endl;
			
			// Clear old project.
			m_textview.setText("");
			m_textview.setCurrentFile("");
			
			// load tree
			m_treeview.openProject( dialog->get_file()->get_path() );
			break;
		}
		
		case Gtk::ResponseType::CANCEL:
		{
			// debug
			// cout << "Cancel clicked." << endl;
			break;
		}
		
		default:
		{
			cout << "Unexpected button clicked." << endl;
			break;
		}
	}
	delete dialog;
}


void scriptED::on_action_save()
{
	// debug
	// cout << "Saving file..." << endl;
	m_textview.saveFile();
}

void scriptED::on_action_export_plaintext()
{
	// debug
	// cout << "Export Plain Text..." << endl;
	m_textview.exportPlainText();
}

void scriptED::on_action_export_fountain()
{
	// debug
	// cout << "Export Fountain..." << endl;
	m_textview.exportFountain();
}

// View

void scriptED::on_action_view_zoomin()
{
	// debug
	// cout << "scriptED::on_action_view_zoomin ..." << endl;
	
	float zoom = m_textview.get_zoomfactor();
	if( zoom < 5.0 )
	{
		zoom += 0.5;
		// Page size
		float width = 800.0 * zoom;
		m_page_box.set_size_request( int(width), -1 );
		m_textview_ScrolledWindow.set_size_request( int(width), -1 );
		m_textview.set_zoomfactor( zoom );
	}
}

void scriptED::on_action_view_zoomout()
{
	// debug
	// cout << "scriptED::on_action_view_zoomout ..." << endl;
	
	float zoom = m_textview.get_zoomfactor();
	if( zoom > 0.5 )
	{
		zoom -= 0.5;
		// Page size
		float width = 800.0 * zoom;
		m_page_box.set_size_request( int(width), -1 );
		m_textview_ScrolledWindow.set_size_request( int(width), -1 );
		m_textview.set_zoomfactor( zoom );
	}
}


// Format

void scriptED::on_action_format_scene()
{
	m_textview.setFormat( TVF_Scene );
}

void scriptED::on_action_format_action()
{
	m_textview.setFormat( TVF_Action );
}

void scriptED::on_action_format_character()
{
	m_textview.setFormat( TVF_Character );
}

void scriptED::on_action_format_parenthetical()
{
	m_textview.setFormat( TVF_Parenthetical );
}

void scriptED::on_action_format_dialogue()
{
	m_textview.setFormat( TVF_Dialogue );
}

void scriptED::on_action_format_transition()
{
	m_textview.setFormat( TVF_Transition );
}

void scriptED::on_action_format_shot()
{
	m_textview.setFormat( TVF_Shot );
}

void scriptED::on_action_format_notes()
{
	m_textview.setFormat( TVF_Notes );
}

void scriptED::on_action_format_plaintext()
{
	m_textview.setFormat( TVF_PlainText );
}

void scriptED::on_action_format_act()
{
	m_textview.setFormat( TVF_Act );
}


// AI lsllamacpp

void scriptED::on_action_ingest_current()
{
	// debug
	// cout << "scriptED::on_action_ingest_current..." << endl;
	
	// Tell user.
	m_lschatai.add_text( "### Import started...\n" );
	m_lschatai.write();
	
	// split scenes with numbers.
	vector<string> scenes = m_textview.split_scenes( false, true );
	m_lschatai.ingest( scenes );
}

void scriptED::on_action_rewrite_selection()
{
	string prompt;
	
	// get selected text.
	prompt = m_textview.getSelection();
	if( prompt.empty() ) return;
	
	// Add instructions.
	prompt += "\nRewrite the above text, to make it more interesting.";
	
	m_lschatai.submit( prompt );
}

void scriptED::on_action_clear_database()
{
	m_lschatai.clear_db();
}

//****************
// Tools

void scriptED::on_action_split_scenes()
{
	// check for scenes folder
	bool exists = fs::exists( m_treeview.get_currentProject() + "/scenes" );
    if(! exists )
	{
		// create it if needed.
		// create folder
		bool val = fs::create_directory( m_treeview.get_currentProject() + "/scenes" );
		if(! val )
		{
			cout << "ERROR : Unable to create folder : " << m_treeview.get_currentProject() << "/scenes" << endl;
			return;
		}
	} else
	{
		// Folder exists. Bail so we don't overwrite something.
		cout << "ERROR : Folder already exists : " << m_treeview.get_currentProject() << "/scenes" << endl;
		return;
	}
	
	// Split current file into scenes, keep formatting and add scene numbers.
	vector<string> scenes = m_textview.split_scenes( true, true );
	if( scenes.empty() )
	{
		m_msg_dialog.set_secondary_text( "No scenes found in current file, please add some formatted scene headers." );
		m_msg_dialog.set_modal( true );
		m_msg_dialog.set_hide_on_close( true );
		m_msg_dialog.show();
		return;
	}
	
	// Save each file.
	int scene_num = 1;
	for( auto scene : scenes )
	{
		// create filename.
		stringstream ss;
		ss << m_treeview.get_currentProject();
		ss << "/scenes/Scene_";
		ss << setfill('0') << setw(3) << scene_num;
		ss << ".scn";
		// debug
		// cout << "Saving file : " << ss.str() << endl;
		// cout << scene << endl << endl;
		
		// save scene file.
		ofstream outfile( ss.str() );
		outfile << scene;
		outfile.close();
		
		scene_num++;
	}
	
	// refrest Project Tree.
	m_treeview.refreshTree();
}

void scriptED::on_action_split_acts()
{
	// check for acts folder
	bool exists = fs::exists( m_treeview.get_currentProject() + "/acts" );
    if(! exists )
	{
		// create folder
		bool val = fs::create_directory( m_treeview.get_currentProject() + "/acts" );
		if(! val )
		{
			cout << "ERROR : Unable to create folder : " << m_treeview.get_currentProject() << "/acts" << endl;
			return;
		}
	} else
	{
		// Folder exists. Bail so we don't overwrite something.
		cout << "ERROR : Folder already exists : " << m_treeview.get_currentProject() << "/acts" << endl;
		return;
	}
	
	// Split current file into acts
	vector<string> acts = m_textview.split_acts();
	if( acts.empty() )
	{
		m_msg_dialog.set_secondary_text( "No acts found in current file, please add some formatted acts." );
		m_msg_dialog.set_modal( true );
		m_msg_dialog.set_hide_on_close( true );
		m_msg_dialog.show();
		return;
	}
	
	// Save each file.
	int act_num = 1;
	for( auto act : acts )
	{
		// create filename.
		stringstream ss;
		ss << m_treeview.get_currentProject();
		ss << "/acts/act_";
		ss << setfill('0') << setw(3) << act_num;
		ss << ".scn";
		// debug
		cout << "Saving file : " << ss.str() << endl;
		
		// save act file.
		ofstream outfile( ss.str() );
		outfile << act;
		outfile.close();
		
		act_num++;
	}
	
	// refrest Project Tree.
	m_treeview.refreshTree();
}

void scriptED::on_action_locations()
{
	// check for locations folder
	bool exists = fs::exists( m_treeview.get_currentProject() + "/locations" );
    if(! exists )
	{
		// create it if needed.
		// create folder
		bool val = fs::create_directory( m_treeview.get_currentProject() + "/locations" );
		if(! val )
		{
			cout << "ERROR : Unable to create folder : " << m_treeview.get_currentProject() << "/locations" << endl;
			return;
		}
	} else
	{
		// Folder exists. Bail so we don't overwrite something.
		cout << "ERROR : Folder already exists : " << m_treeview.get_currentProject() << "/locations" << endl;
		return;
	}
	
	// Split current file into locations
	vector<string> locations = m_textview.get_locations();
	if( locations.empty() )
	{
		m_msg_dialog.set_secondary_text( "No locations found in current file, please add some formatted scene headers." );
		m_msg_dialog.set_modal( true );
		m_msg_dialog.set_hide_on_close( true );
		m_msg_dialog.show();
		return;
	}
	
	// Build file contents.
	stringstream ss;
	for( const auto& line : locations )
	{
		ss << line << endl;
	}
	ss << endl;
	
	// locations filename.
	string filename = m_treeview.get_currentProject() + "/locations/" + "locations.txt";
	// debug
	cout << "Saving file : " << filename << endl;
	
	// save scene file.
	ofstream outfile( filename );
	outfile << ss.str();
	outfile.close();
	
	// refrest Project Tree.
	m_treeview.refreshTree();
}

void scriptED::on_action_characters()
{
	// check for characters folder
	bool exists = fs::exists( m_treeview.get_currentProject() + "/characters" );
    if(! exists )
	{
		// create it if needed.
		// create folder
		bool val = fs::create_directory( m_treeview.get_currentProject() + "/characters" );
		if(! val )
		{
			cout << "ERROR : Unable to create folder : " << m_treeview.get_currentProject() << "/characters" << endl;
			return;
		}
	} else
	{
		// Folder exists. Bail so we don't overwrite something.
		cout << "ERROR : Folder already exists : " << m_treeview.get_currentProject() << "/characters" << endl;
		return;
	}
	
	// Split current file into locations
	vector<string> characters = m_textview.get_characters();
	if( characters.empty() )
	{
		m_msg_dialog.set_secondary_text( "No characters found in current file, please add some formatted characters." );
		m_msg_dialog.set_modal( true );
		m_msg_dialog.set_hide_on_close( true );
		m_msg_dialog.show();
		return;
	}
	
	// Build file contents.
	stringstream ss;
	for( const auto& line : characters )
	{
		ss << line << endl;
	}
	ss << endl;
	
	// locations filename.
	string filename = m_treeview.get_currentProject() + "/characters/" + "characters.txt";
	// debug
	cout << "Saving file : " << filename << endl;
	
	// save characters file.
	ofstream outfile( filename );
	outfile << ss.str();
	outfile.close();
	
	// refrest Project Tree.
	m_treeview.refreshTree();
}

