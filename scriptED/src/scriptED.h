#ifndef SCRIPTED_H
#define SCRIPTED_H

#include <iostream>
#include <sstream>
#include <fstream>  

// gtkmm
#include <gtkmm.h>
// glibmm
#include <glibmm.h>

// lsChatAI
#include <lsChatAI_gtkmm/lsChatAI.h>

#include "TreeView.h"
#include "TextView.h"


class scriptED : public Gtk::Window
{
public:
	scriptED( Glib::RefPtr<Gtk::Application> a );
	virtual ~scriptED();
	
private:
	Glib::RefPtr<Gtk::Application> app;
	
	//Child widgets
	Gtk::Box m_VBox;
	Gtk::Paned m_HPaned;
	Gtk::Paned m_HPanedAI;
	
	// Overrides
	void on_realize();
	
	// Signals
	// File
	void on_action_openproject();
	void on_action_save();
	void on_action_export_plaintext();
	void on_action_export_fountain();
	// View
	void on_action_view_zoomin();
	void on_action_view_zoomout();
	// Format
	void on_action_format_scene();
	void on_action_format_action();
	void on_action_format_character();
	void on_action_format_parenthetical();
	void on_action_format_dialogue();
	void on_action_format_transition();
	void on_action_format_shot();
	void on_action_format_notes();
	void on_action_format_plaintext();
	void on_action_format_act();
	// Ai
	void on_action_ingest_current();
	void on_action_rewrite_selection();
	void on_action_clear_database();
	// Tools
	void on_action_split_scenes();
	void on_action_split_acts();
	void on_action_locations();
	void on_action_characters();
	
	// dialogs
	Gtk::MessageDialog m_msg_dialog;
	// responces
	void on_folder_dialog_response( int response_id, Gtk::FileChooserDialog* dialog );
	
	// Scroll windows.
	Gtk::ScrolledWindow m_textview_ScrolledWindow;
	TreeView m_treeview;
	Gtk::Box m_page_box;
	TextView m_textview;
	
	// lsChatAI
	lsChatAI m_lschatai;
};

#endif // SCRIPTED_H
