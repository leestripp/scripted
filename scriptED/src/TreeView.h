#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <iostream>
#include <filesystem>
#include <fstream>

#include <gtkmm.h>

// dialogs.
#include "dialogs/scriptED_TextEntry.h"

using namespace std;
namespace fs = std::filesystem;

// Icons
#include "icons/folder_16x16.xpm"
#include "icons/file_16x16.xpm"

// row types
const int TVRT_UNKNOWN 	= 0;
const int TVRT_DIR 		= 1;
const int TVRT_FILE 	= 2;
const int TVRT_PROJECT 	= 3;

// protos
class TextView;


class TreeView : public Gtk::ScrolledWindow
{
public:
	TreeView();
	virtual ~TreeView();
	
	void openProject( const std::string &path );
	void refreshTree();
	void loadIcons();
	
	// Get Set
	void set_TextView( TextView *val )
	{
		m_textview = val;
	}
	
	const string& get_currentProject()
	{
		return m_currentProject;
	}
	
private:
	Gtk::TreeView m_treeview;
	
	// Tree model columns
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{
			add( m_col_icon );
			add( m_col_name );
			add( m_full_path );
			add( m_type );
		}
		Gtk::TreeModelColumn<Glib::RefPtr<Gdk::Pixbuf>> m_col_icon;
		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
		Gtk::TreeModelColumn<std::string> m_full_path;
		Gtk::TreeModelColumn<int> m_type;
	};
	ModelColumns m_Columns;
	
	// Signals
	void on_treeview_row_activated( const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column );
	void on_popup_menu( int n_press, double x, double y );
	
	// Popup Menu
	void on_popup_new_file();
	void on_popup_new_folder();
	void on_popup_remove();
	void on_popup_refresh();
	void on_popup_ingest_scenes();
	
	// Menbers
	void loadTree( const std::string &path, Gtk::TreeModel::Row row, int lvl );
	
	// TextView
	TextView *m_textview;
	
	// Icons
	Glib::RefPtr<Gdk::Pixbuf> m_icon_folder;
	Glib::RefPtr<Gdk::Pixbuf> m_icon_file;
	
	// Data
	Glib::RefPtr<Gtk::TreeStore> m_refTreeModel;
	string m_currentProject;
	
	// Popup menu
	Gtk::PopoverMenu *m_MenuPopup;
	
	// Dialogs
	scriptED_TextEntry m_dialog_file;
	scriptED_TextEntry m_dialog_dir;
	// dialog signals
	void on_dialog_file_response( const Glib::ustring& response );
	void on_dialog_dir_response( const Glib::ustring& response );
};

#endif // TREEVIEW_H
