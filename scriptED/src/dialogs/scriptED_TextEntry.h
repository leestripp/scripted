#ifndef SCRIPTED_TEXTENTRY_H
#define SCRIPTED_TEXTENTRY_H

#include <iostream>

// gtkmm
#include <gtkmm.h>
// glibmm
#include <glibmm.h>

using namespace std;


class scriptED_TextEntry : public Gtk::Window
{
public:
	scriptED_TextEntry();
	virtual ~scriptED_TextEntry();
	
	void buttons_clicked_connect( const sigc::slot<void(const Glib::ustring&)>& slot );
	Glib::ustring get_entry();
	
private:
	//Member widgets:
	Gtk::Grid m_Grid;
	Gtk::Image m_Image;
	Gtk::Label m_Label;
	Gtk::Entry m_Entry;
	Gtk::Box m_ButtonBox;
	Gtk::Button m_Button_OK;
	Gtk::Button m_Button_Cancel;
};

#endif // SCRIPTED_TEXTENTRY_H