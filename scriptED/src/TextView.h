#ifndef TEXTVIEW_H
#define TEXTVIEW_H

#include <iostream>
#include <fstream>
#include <locale>

#include <gtkmm.h>

using namespace std;

// Formats
const int TVF_Scene			= 0;
const int TVF_Action		= 1;
const int TVF_Character		= 2;
const int TVF_Parenthetical	= 3;
const int TVF_Dialogue		= 4;
const int TVF_Transition	= 5;
const int TVF_Shot	 		= 6;
const int TVF_Notes	 		= 7;
const int TVF_PlainText		= 8;
const int TVF_Act			= 9;


class TextView : public Gtk::TextView
{
public:
	TextView();
	
	void loadFile( const string &file );
	void saveFile();
	void exportPlainText();
	void exportFountain();
	
	void setText( const Glib::ustring& s );
	Glib::ustring getText();
	Glib::ustring getSelection();
	void setFormat( int f );
	void setLineFormat( int f );
	void updateTags();
	void set_zoomfactor( float val );
	vector<string> split_scenes( bool add_format=true, bool add_scene_nums=false );
	vector<string> split_acts();
	vector<string> get_locations();
	vector<string> get_characters();
	
	// Get set
	void setCurrentFile( const string &val )
	{
		m_currentFile = val;
	}
	const string& getCurrentFile()
	{
		return m_currentFile;
	}
	
	float get_zoomfactor()
	{
		return m_zoomFactor;
	}
	
private:
	Glib::RefPtr<Gtk::CssProvider> m_provider;
	
	string m_currentFile;
	int m_currentFormat;
	int m_currentLine;
	
	// Format Tags
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Scene;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Action;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Character;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Parenthetical;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Dialogue;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Transition;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Shot;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Notes;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_PlainText;
	Glib::RefPtr<Gtk::TextBuffer::Tag> m_tag_Act;
	
	// Zoom factor
	float m_zoomFactor;
	
};

#endif // TEXTVIEW_H

