#include "TextView.h"
#include "css/css_editor.h"

TextView::TextView()
{
	m_currentFile = "";
	m_currentLine = 0;
	m_currentFormat = TVF_PlainText;
	m_zoomFactor = 2.0;
	
	// Expand to fill the page area.
	set_vexpand( true );
	
	// CSS
	m_provider = Gtk::CssProvider::create();
    m_provider->load_from_data(	css_editor );
	set_name( "Editor" );
	get_style_context()->add_provider( m_provider, 1 );
	
	// Set some defaults
	property_wrap_mode() = Gtk::WrapMode::WORD;
	property_justification() = Gtk::Justification::LEFT;
	
	// Create Tags
	m_tag_Scene = Gtk::TextBuffer::Tag::create();
	m_tag_Action = Gtk::TextBuffer::Tag::create();
	m_tag_Character = Gtk::TextBuffer::Tag::create();
	m_tag_Parenthetical = Gtk::TextBuffer::Tag::create();
	m_tag_Dialogue = Gtk::TextBuffer::Tag::create();
	m_tag_Transition = Gtk::TextBuffer::Tag::create();
	m_tag_Shot = Gtk::TextBuffer::Tag::create();
	m_tag_Notes = Gtk::TextBuffer::Tag::create();
	m_tag_PlainText = Gtk::TextBuffer::Tag::create();
	m_tag_Act = Gtk::TextBuffer::Tag::create();
	
	// Set tag properties.
	updateTags();
	
	// Add Tags to table
	auto refTagTable = get_buffer()->get_tag_table();
	refTagTable->add( m_tag_Scene );
	refTagTable->add( m_tag_Action );
	refTagTable->add( m_tag_Character );
	refTagTable->add( m_tag_Parenthetical );
	refTagTable->add( m_tag_Dialogue );
	refTagTable->add( m_tag_Transition );
	refTagTable->add( m_tag_Shot );
	refTagTable->add( m_tag_Notes );
	refTagTable->add( m_tag_PlainText );
	refTagTable->add( m_tag_Act );
}


void TextView::exportPlainText()
{
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant save." << endl;
		return;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			// not empty line
			if( tmp_iter.get_char() != '\n' )
			{
				lineStr = get_buffer()->get_text( iter, iter_end );
				
				if( iter.has_tag( m_tag_Character ) )
				{
					ss << endl << lineStr << endl;
				} else if( iter.has_tag( m_tag_Dialogue ) )
				{
					ss << lineStr << endl << endl;
				} else if( iter.has_tag( m_tag_Scene ) )
				{
					ss << endl << endl << lineStr << endl;
				} else
				{
					ss << lineStr << endl;
				}
			} else
			{
				// Empty line
				ss << endl;
			}
		}
		// debug
		// cout << "line:" << line << " - " << lineStr << endl;
		
		// Check if current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = get_buffer()->get_text( iter, iter_end );
			ss << lineStr << endl;
			break;
		}
		
		line++;
		iter.set_line( line );
	}
	
	ofstream ofs;
	ofs.open( m_currentFile + ".txt" );
	if( ofs.is_open() )
	{
		// debug
		// cout << ss.str();
		ofs << ss.str();
		ofs.close();
	}
}


void TextView::exportFountain()
{
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant save." << endl;
		return;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			// not empty line
			if( tmp_iter.get_char() != '\n' )
			{
				lineStr = get_buffer()->get_text( iter, iter_end );
				
				if( iter.has_tag( m_tag_Scene ) )
				{
					ss << endl << "." << lineStr << endl << endl;
				} else if( iter.has_tag( m_tag_Action ) )
				{
					ss << "!" << lineStr << endl;
				} else if( iter.has_tag( m_tag_Character ) )
				{
					ss << endl << "@" << lineStr << endl;
				} else if( iter.has_tag( m_tag_Parenthetical ) )
				{
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Dialogue ) )
				{
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Transition ) )
				{
					ss << ">" << lineStr << endl;
				} else if( iter.has_tag( m_tag_Act ) )
				{
					ss << "===" << endl << ">***" << lineStr << "***<" << endl;
				}  else
				{
					ss << lineStr << endl;
				}
			} else
			{
				// Empty line
				ss << endl;
			}
		}
		// debug
		// cout << "line:" << line << " - " << lineStr << endl;
		
		// Check if current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = get_buffer()->get_text( iter, iter_end );
			ss << lineStr << endl;
			break;
		}
		
		line++;
		iter.set_line( line );
	}
	
	ofstream ofs;
	ofs.open( m_currentFile + ".fountain" );
	if( ofs.is_open() )
	{
		// debug
		// cout << ss.str();
		ofs << ss.str();
		ofs.close();
	}
}


void TextView::updateTags()
{
	// CSS
	stringstream ss;
	ss << "#Editor{" << endl;
	ss << "font-size: " << 12 * m_zoomFactor << "px;" << endl;
	ss << "}";
    m_provider->load_from_data(	ss.str() );
	
	m_tag_Scene->property_foreground_rgba() = Gdk::RGBA( 1, 1, 0 );
	m_tag_Scene->property_left_margin() = 5 * m_zoomFactor;
	m_tag_Scene->property_right_margin() = 5 * m_zoomFactor;
	m_tag_Scene->property_weight() = Pango::Weight::BOLD;
	m_tag_Scene->property_size() = (15*Pango::SCALE) * m_zoomFactor;
	m_tag_Scene->property_pixels_above_lines() = 20 * m_zoomFactor;
	
	m_tag_Action->property_foreground_rgba() = Gdk::RGBA( 1, 1, 1 );
	m_tag_Action->property_left_margin() = 20 * m_zoomFactor;
	m_tag_Action->property_right_margin() = 20 * m_zoomFactor;
	m_tag_Action->property_size() = (12*Pango::SCALE) * m_zoomFactor;
	m_tag_Action->property_pixels_above_lines() = 5 * m_zoomFactor;
	
	m_tag_Character->property_foreground_rgba() = Gdk::RGBA( 1, 0.5, 0.2 );
	m_tag_Character->property_left_margin() = 350 * m_zoomFactor;
	m_tag_Character->property_right_margin() = 20 * m_zoomFactor;
	m_tag_Character->property_weight() = Pango::Weight::BOLD;
	m_tag_Character->property_size() = (15*Pango::SCALE) * m_zoomFactor;
	m_tag_Character->property_pixels_above_lines() = 30 * m_zoomFactor;
	
	m_tag_Parenthetical->property_foreground_rgba() = Gdk::RGBA( 0.7, 0.7, 0.5 );
	m_tag_Parenthetical->property_left_margin() = 300 * m_zoomFactor;
	m_tag_Parenthetical->property_right_margin() = 20 * m_zoomFactor;
	m_tag_Parenthetical->property_size() = (12*Pango::SCALE) * m_zoomFactor;
	m_tag_Parenthetical->property_pixels_above_lines() = 0;
	
	m_tag_Dialogue->property_foreground_rgba() = Gdk::RGBA( 1, 1, 1 );
	m_tag_Dialogue->property_left_margin() = 200 * m_zoomFactor;
	m_tag_Dialogue->property_right_margin() = 200 * m_zoomFactor;
	m_tag_Dialogue->property_size() = (12*Pango::SCALE) * m_zoomFactor;
	m_tag_Dialogue->property_pixels_above_lines() = 0;
	m_tag_Dialogue->property_pixels_below_lines() = 20 * m_zoomFactor;
	
	m_tag_Transition->property_foreground_rgba() = Gdk::RGBA( 0.5, 1, 0.5 );
	m_tag_Transition->property_left_margin() = 10 * m_zoomFactor;
	m_tag_Transition->property_right_margin() = 10 * m_zoomFactor;
	m_tag_Transition->property_size() = (12*Pango::SCALE) * m_zoomFactor;
	m_tag_Transition->property_pixels_above_lines() = 10 * m_zoomFactor;
	m_tag_Transition->property_justification() = Gtk::Justification::RIGHT;
	
	m_tag_Shot->property_foreground_rgba() = Gdk::RGBA( 0.5, 0.5, 1 );
	m_tag_Shot->property_left_margin() = 10 * m_zoomFactor;
	m_tag_Shot->property_right_margin() = 10 * m_zoomFactor;
	m_tag_Shot->property_size() = (12*Pango::SCALE) * m_zoomFactor;
	m_tag_Shot->property_pixels_above_lines() = 10 * m_zoomFactor;
	
	m_tag_Notes->property_foreground_rgba() = Gdk::RGBA( 1, 1, 0.5 );
	m_tag_Notes->property_left_margin() = 10 * m_zoomFactor;
	m_tag_Notes->property_right_margin() = 10 * m_zoomFactor;
	m_tag_Notes->property_style() = Pango::Style::ITALIC;
	m_tag_Notes->property_size() = (10*Pango::SCALE) * m_zoomFactor;
	m_tag_Notes->property_pixels_above_lines() = 10 * m_zoomFactor;
	
	m_tag_PlainText->property_foreground_rgba() = Gdk::RGBA( 1, 1, 1 );
	m_tag_PlainText->property_left_margin() = 10 * m_zoomFactor;
	m_tag_PlainText->property_right_margin() = 10 * m_zoomFactor;
	m_tag_PlainText->property_size() = (10*Pango::SCALE) * m_zoomFactor;
	m_tag_PlainText->property_pixels_above_lines() = 10 * m_zoomFactor;
	
	m_tag_Act->property_foreground_rgba() = Gdk::RGBA( 1, 0.5, 1 );
	m_tag_Act->property_left_margin() = 10 * m_zoomFactor;
	m_tag_Act->property_right_margin() = 10 * m_zoomFactor;
	m_tag_Act->property_weight() = Pango::Weight::BOLD;
	m_tag_Act->property_style() = Pango::Style::ITALIC;
	m_tag_Act->property_size() = (14*Pango::SCALE) * m_zoomFactor;
	m_tag_Act->property_pixels_above_lines() = 10 * m_zoomFactor;
}

void TextView::setText( const Glib::ustring& s )
{
	get_buffer()->set_text( s );
}

Glib::ustring TextView::getText()
{
	// buffer
	auto input_buffer = get_buffer();
	
	int line = 0;
	stringstream ss;
	string lineStr;
	string output;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = input_buffer->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			lineStr = input_buffer->get_text( iter, iter_end );
			// save line
			output += lineStr + "\n";
		}
		
		// Check if current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = input_buffer->get_text( iter, iter_end );
			output += lineStr;
			break;
		}
		
		// next line
		line++;
		iter.set_line( line );
	}// while
	
	return output;
}

Glib::ustring TextView::getSelection()
{
	// buffer
	auto input_buffer = get_buffer();
	Glib::ustring output;
	if( input_buffer->get_has_selection() )
	{
		// bool get_selection_bounds( const_iterator &range_start, const_iterator &range_end )
		Gtk::TextBuffer::const_iterator range_start, range_end;
		input_buffer->get_selection_bounds( range_start, range_end );
		output = input_buffer->get_text( range_start, range_end );
	}
	return output;
}


void TextView::loadFile( const string &file )
{
	// Clear buffer
	get_buffer()->set_text( "" );
	
	ifstream ifs;
	string line;
	Glib::RefPtr<Gtk::TextBuffer::Tag> tag;
	
	ifs.open( file );
	if( ifs.is_open() )
	{
		auto buffer = get_buffer();
		tag = NULL;
		
		while( getline( ifs, line ) )
		{
			if( line == "#tag Scene" )
			{
				tag = m_tag_Scene;
				m_currentFormat = TVF_Scene;
				
			} else if( line == "#tag Action" )
			{
				tag = m_tag_Action;
				m_currentFormat = TVF_Action;
				
			} else if( line == "#tag Character" )
			{
				tag = m_tag_Character;
				m_currentFormat = TVF_Character;
				
			} else if( line == "#tag Parenthetical" )
			{
				tag = m_tag_Parenthetical;
				m_currentFormat = TVF_Parenthetical;
				
			} else if( line == "#tag Dialogue" )
			{
				tag = m_tag_Dialogue;
				m_currentFormat = TVF_Dialogue;
				
			} else if( line == "#tag Transition" )
			{
				tag = m_tag_Transition;
				m_currentFormat = TVF_Transition;
				
			} else if( line == "#tag Shot" )
			{
				tag = m_tag_Shot;
				m_currentFormat = TVF_Shot;
				
			} else if( line == "#tag Notes" )
			{
				tag = m_tag_Notes;
				m_currentFormat = TVF_Notes;
				
			} else if( line == "#tag PlainText" )
			{
				tag = m_tag_PlainText;
				m_currentFormat = TVF_PlainText;
				
			} else if( line == "#tag Act" )
			{
				tag = m_tag_Act;
				m_currentFormat = TVF_Act;
				
			} else
			{
				// Insert line
				if( tag )
				{
					buffer->insert_with_tag( buffer->end(), line, tag );
					setFormat( m_currentFormat );
					tag = NULL;
				} else
				{
					buffer->insert( buffer->end(), line );
				}
				buffer->insert( buffer->end(), "\n" );
			}
		} // while getline
		
		ifs.close();
		// Store current file path.
		m_currentFile = file;
	}
}

void TextView::saveFile()
{
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant save." << endl;
		return;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			if( tmp_iter.get_char() != '\n' )
			{
				if( iter.has_tag( m_tag_Scene ) )
				{
					ss << "#tag Scene" << endl;
				} else if( iter.has_tag( m_tag_Action ) )
				{
					ss << "#tag Action" << endl;
				} else if( iter.has_tag( m_tag_Character ) )
				{
					ss << "#tag Character" << endl;
				} else if( iter.has_tag( m_tag_Parenthetical ) )
				{
					ss << "#tag Parenthetical" << endl;
				} else if( iter.has_tag( m_tag_Dialogue ) )
				{
					ss << "#tag Dialogue" << endl;
				} else if( iter.has_tag( m_tag_Transition ) )
				{
					ss << "#tag Transition" << endl;
				} else if( iter.has_tag( m_tag_Shot ) )
				{
					ss << "#tag Shot" << endl;
				} else if( iter.has_tag( m_tag_Notes ) )
				{
					ss << "#tag Notes" << endl;
				} else if( iter.has_tag( m_tag_PlainText ) )
				{
					ss << "#tag PlainText" << endl;
				} else if( iter.has_tag( m_tag_Act ) )
				{
					ss << "#tag Act" << endl;
				}
				
				lineStr = get_buffer()->get_text( iter, iter_end );
				ss << lineStr << endl;
			} else
			{
				// Empty line
				ss << endl;
			}
		}
		// debug
		// cout << "line:" << line << " - " << lineStr << endl;
		
		// Check current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = get_buffer()->get_text( iter, iter_end );
			if( iter.has_tag( m_tag_Scene ) )
			{
				ss << "#tag Scene" << endl;
			} else if( iter.has_tag( m_tag_Action ) )
			{
				ss << "#tag Action" << endl;
			} else if( iter.has_tag( m_tag_Character ) )
			{
				ss << "#tag Character" << endl;
			} else if( iter.has_tag( m_tag_Parenthetical ) )
			{
				ss << "#tag Parenthetical" << endl;
			} else if( iter.has_tag( m_tag_Dialogue ) )
			{
				ss << "#tag Dialogue" << endl;
			} else if( iter.has_tag( m_tag_Transition ) )
			{
				ss << "#tag Transition" << endl;
			} else if( iter.has_tag( m_tag_Shot ) )
			{
				ss << "#tag Shot" << endl;
			} else if( iter.has_tag( m_tag_Notes ) )
			{
				ss << "#tag Notes" << endl;
			} else if( iter.has_tag( m_tag_PlainText ) )
			{
				ss << "#tag PlainText" << endl;
			} else if( iter.has_tag( m_tag_Act ) )
			{
				ss << "#tag Act" << endl;
			}
			ss << lineStr << endl;
			break;
		}
		
		line++;
		iter.set_line( line );
	}
	
	ofstream ofs;
	ofs.open( m_currentFile );
	if( ofs.is_open() )
	{
		// debug
		// cout << ss.str();
		ofs << ss.str();
		ofs.close();
	}
}


void TextView::setFormat( int f )
{
	setLineFormat( f );
}

void TextView::setLineFormat( int f )
{
	m_currentFormat = f;
	int line_num = 0;
	
	// get line start and cursor pos
	Glib::RefPtr<Gtk::TextBuffer::Mark> ins_mark = get_buffer()->get_insert();
	auto iter_end = ins_mark->get_iter();
	line_num = iter_end.get_line();
	if(! iter_end.ends_line() ) iter_end.forward_to_line_end();
	auto iter_start = get_buffer()->get_iter_at_line( iter_end.get_line() );
	
	// Clear current Tags
	get_buffer()->remove_all_tags( iter_start, iter_end );
	string lineStr;
	
	switch( m_currentFormat )
	{
		case TVF_Scene:
		{
			// get line
			lineStr = get_buffer()->get_text( iter_start, iter_end );
			transform( lineStr.begin(), lineStr.end(), lineStr.begin(), [](char c) { return toupper(c); });
			// debug
			// cout << lineStr << endl;
			// replace line
			iter_start = get_buffer()->erase( iter_start, iter_end );
			iter_start.set_line( line_num );
			get_buffer()->insert_with_tag( iter_start, lineStr, m_tag_Scene );
			break;
		}
		
		case TVF_Action:
		{
			get_buffer()->apply_tag( m_tag_Action, iter_start, iter_end );
			break;
		}
		
		case TVF_Character:
		{
			// get line
			lineStr = get_buffer()->get_text( iter_start, iter_end );
			transform( lineStr.begin(), lineStr.end(), lineStr.begin(), [](char c) { return toupper(c); });
			// debug
			// cout << lineStr << endl;
			// replace line
			iter_start = get_buffer()->erase( iter_start, iter_end );
			iter_start.set_line( line_num );
			get_buffer()->insert_with_tag( iter_start, lineStr, m_tag_Character );
			break;
		}
		
		case TVF_Parenthetical:
		{
			get_buffer()->apply_tag( m_tag_Parenthetical, iter_start, iter_end );
			break;
		}
		
		case TVF_Dialogue:
		{
			get_buffer()->apply_tag( m_tag_Dialogue, iter_start, iter_end );
			break;
		}
		
		case TVF_Transition:
		{
			// get line
			lineStr = get_buffer()->get_text( iter_start, iter_end );
			transform( lineStr.begin(), lineStr.end(), lineStr.begin(), [](char c) { return toupper(c); });
			// debug
			// cout << lineStr << endl;
			// replace line
			iter_start = get_buffer()->erase( iter_start, iter_end );
			iter_start.set_line( line_num );
			get_buffer()->insert_with_tag( iter_start, lineStr, m_tag_Transition );
			break;
		}
		
		case TVF_Shot:
		{
			get_buffer()->apply_tag( m_tag_Shot, iter_start, iter_end );
			break;
		}
		
		case TVF_Notes:
		{
			get_buffer()->apply_tag( m_tag_Notes, iter_start, iter_end );
			break;
		}
		
		case TVF_PlainText:
		{
			get_buffer()->apply_tag( m_tag_PlainText, iter_start, iter_end );
			break;
		}
		
		case TVF_Act:
		{
			get_buffer()->apply_tag( m_tag_Act, iter_start, iter_end );
			break;
		}
	}
}

void TextView::set_zoomfactor( float val )
{
	m_zoomFactor = val;
	// Page size
	float width = 800.0 * m_zoomFactor;
	set_size_request( int(width), -1 );
	updateTags();
}

// ****************
// Split

vector<string> TextView::split_scenes( bool add_format, bool add_scene_nums )
{
	// debug
	cout << "*******************************************************" << endl;
	cout << "TextView::split_scenes( bool add_format ) - Called" << endl;
	
	vector<string> scenes;
	int scene_num = 0;
	
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant split scenes." << endl;
		return scenes;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while( 1 )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		iter_end.forward_to_line_end();
		lineStr = get_buffer()->get_text( iter, iter_end );
		
		// Check if current line is the end.
		if( iter_end.is_end() )
		{
			// Add last line.
			ss << lineStr << endl;
			
			// store last header
			scenes.push_back( ss.str() );
			break;
		}
		
		if( tmp_iter.get_char() == '\n' )
		{
			ss << endl;
			
			line++;
			iter.set_line( line );
			continue;
		}
		
		// Tags
		if( iter.has_tag( m_tag_Scene ) )
		{
			if( scene_num > 0 )
			{
				// store last scene
				scenes.push_back( ss.str() );
				
				// debug
				// cout << ss.str() << endl;
			}
			// clear the stream
			ss.str( string() );
			
			scene_num++;
			if( add_format ) ss << "#tag Scene" << endl;
			if( add_scene_nums ) ss << "Scene " << scene_num << " : ";
			ss << lineStr << endl;
			
		} else if( iter.has_tag( m_tag_Action ) )
		{
			if( add_format ) ss << "#tag Action" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Character ) )
		{
			if( add_format ) ss << "#tag Character" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Parenthetical ) )
		{
			if( add_format ) ss << "#tag Parenthetical" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Dialogue ) )
		{
			if( add_format ) ss << "#tag Dialogue" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Transition ) )
		{
			if( add_format ) ss << "#tag Transition" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Shot ) )
		{
			if( add_format ) ss << "#tag Shot" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Notes ) )
		{
			if( add_format ) ss << "#tag Notes" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_PlainText ) )
		{
			if( add_format ) ss << "#tag PlainText" << endl;
			ss << lineStr << endl;
		} else if( iter.has_tag( m_tag_Act ) )
		{
			if( add_format ) ss << "#tag Act" << endl;
			ss << lineStr << endl;
		} else
		{
			// Unformatted line
			ss << lineStr << endl;
		}
		
		line++;
		iter.set_line( line );
		
	}// while( 1 )
	
	// debug
	cout << "Number of scenes : " << scenes.size() << endl;
	
	return scenes;
}


vector<string> TextView::get_locations()
{
	vector<string> scenes;
	int scene_num = 0;
	
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant create locations." << endl;
		return scenes;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			if( tmp_iter.get_char() != '\n' )
			{
				lineStr = get_buffer()->get_text( iter, iter_end );
				
				if( iter.has_tag( m_tag_Scene ) )
				{
					if( scene_num > 0 )
					{
						// store last scene
						scenes.push_back( ss.str() );
						
						// debug
						// cout << ss.str() << endl;
					}
					// clear the stream
					ss.str( string() );
					
					scene_num++;
					ss << "#tag Scene" << endl;
					ss << "S" << scene_num << " " << lineStr << endl;
					
				} else if( iter.has_tag( m_tag_Notes ) )
				{
					ss << "#tag Notes" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_PlainText ) )
				{
					ss << "#tag PlainText" << endl;
					ss << lineStr << endl;
				}
				
			} else
			{
				// Empty line
				ss << endl;
			}
			
		} // if( iter_end.forward_to_line_end() )
		
		// Check current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = get_buffer()->get_text( iter, iter_end );
			
			if( iter.has_tag( m_tag_Scene ) )
			{
				if( scene_num > 0 )
				{
					// store last scene
					scenes.push_back( ss.str() );
					
					// debug
					// cout << ss.str() << endl;
				}
				// clear the stream
				ss.str(std::string());
				
				ss << "#tag Scene" << endl;
				ss << "S" << scene_num << " " << lineStr << endl;
				
			} else if( iter.has_tag( m_tag_Notes ) )
			{
				ss << "#tag Notes" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_PlainText ) )
			{
				ss << "#tag PlainText" << endl;
				ss << lineStr << endl;
			}
			
			if( scene_num > 0 )
			{
				// store last scene
				scenes.push_back( ss.str() );
				
				// debug
				// cout << ss.str() << endl;
			}
			break;
		} // if( iter_end.is_end() )
		
		line++;
		iter.set_line( line );
		
	}// while(! iter.is_end() )
	
	// catch single scene
	if( scene_num > 0 )
	{
		// store last scene
		scenes.push_back( ss.str() );
		
		// debug
		// cout << ss.str() << endl;
	}
	
	return scenes;
}


vector<string> TextView::get_characters()
{
	vector<string> scenes;
	int scene_num = 0;
	
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant create characters list." << endl;
		return scenes;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			if( tmp_iter.get_char() != '\n' )
			{
				lineStr = get_buffer()->get_text( iter, iter_end );
				
				if( iter.has_tag( m_tag_Scene ) )
				{
					if( scene_num > 0 )
					{
						// store last scene
						scenes.push_back( ss.str() );
						
						// debug
						// cout << ss.str() << endl;
					}
					// clear the stream
					ss.str( string() );
					
					scene_num++;
					ss << "#tag Scene" << endl;
					ss << "S" << scene_num << " " << lineStr << endl;
					
				} else if( iter.has_tag( m_tag_Character ) )
				{
					ss << "#tag Character" << endl;
					ss << lineStr << endl;
				}  else if( iter.has_tag( m_tag_Notes ) )
				{
					ss << "#tag Notes" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_PlainText ) )
				{
					ss << "#tag PlainText" << endl;
					ss << lineStr << endl;
				}
				
			} else
			{
				// Empty line
				ss << endl;
			}
			
		} // if( iter_end.forward_to_line_end() )
		
		// Check current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = get_buffer()->get_text( iter, iter_end );
			
			if( iter.has_tag( m_tag_Scene ) )
			{
				if( scene_num > 0 )
				{
					// store last scene
					scenes.push_back( ss.str() );
					
					// debug
					// cout << ss.str() << endl;
				}
				// clear the stream
				ss.str(std::string());
				
				ss << "#tag Scene" << endl;
				ss << "S" << scene_num << " " << lineStr << endl;
				
			} else if( iter.has_tag( m_tag_Character ) )
			{
				ss << "#tag Character" << endl;
				ss << lineStr << endl;
			}  else if( iter.has_tag( m_tag_Notes ) )
			{
				ss << "#tag Notes" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_PlainText ) )
			{
				ss << "#tag PlainText" << endl;
				ss << lineStr << endl;
			}
			
			if( scene_num > 0 )
			{
				// store last scene
				scenes.push_back( ss.str() );
				
				// debug
				// cout << ss.str() << endl;
			}
			break;
		} // if( iter_end.is_end() )
		
		line++;
		iter.set_line( line );
		
	}// while(! iter.is_end() )
	
	// catch single scene
	if( scene_num > 0 )
	{
		// store last scene
		scenes.push_back( ss.str() );
		
		// debug
		// cout << ss.str() << endl;
	}
	
	return scenes;
}


vector<string> TextView::split_acts()
{
	vector<string> acts;
	int act_num = 0;
	
	if( m_currentFile == "" )
	{
		cout << "Note : m_currentFile - empty, cant split acts." << endl;
		return acts;
	}
	
	int line = 0;
	stringstream ss;
	string lineStr;
	Gtk::TextIter iter_end, tmp_iter;
	Gtk::TextIter iter = get_buffer()->get_iter_at_line( line );
	while(! iter.is_end() )
	{
		lineStr = "";
		
		tmp_iter = iter_end = iter;
		if( iter_end.forward_to_line_end() )
		{
			if( tmp_iter.get_char() != '\n' )
			{
				lineStr = get_buffer()->get_text( iter, iter_end );
				
				if( iter.has_tag( m_tag_Act ) )
				{
					if( act_num > 0 )
					{
						// store last Act
						acts.push_back( ss.str() );
						
						// debug
						// cout << endl << "### Act " << act_num << endl;
						// cout << ss.str() << endl;
					}
					// clear the stream
					ss.str( string() );
					
					act_num++;
					ss << "#tag Act" << endl;
					ss << lineStr << endl;
					
				} else if( iter.has_tag( m_tag_Action ) )
				{
					ss << "#tag Action" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Character ) )
				{
					ss << "#tag Character" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Parenthetical ) )
				{
					ss << "#tag Parenthetical" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Dialogue ) )
				{
					ss << "#tag Dialogue" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Transition ) )
				{
					ss << "#tag Transition" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Shot ) )
				{
					ss << "#tag Shot" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Notes ) )
				{
					ss << "#tag Notes" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_PlainText ) )
				{
					ss << "#tag PlainText" << endl;
					ss << lineStr << endl;
				} else if( iter.has_tag( m_tag_Scene ) )
				{
					ss << "#tag Scene" << endl;
					ss << lineStr << endl;
				} else
				{
					// Unformatted line
					ss << lineStr << endl;
				}
				
			} else
			{
				// Empty line
				ss << endl;
			}
			
		} // if( iter_end.forward_to_line_end() )
		
		// Check current line is the end.
		if( iter_end.is_end() )
		{
			lineStr = get_buffer()->get_text( iter, iter_end );
			
			if( iter.has_tag( m_tag_Act ) )
			{
				if( act_num > 0 )
				{
					// store last act
					acts.push_back( ss.str() );
					
					// debug
					// cout << ss.str() << endl;
				}
				// clear the stream
				ss.str(std::string());
				
				ss << "#tag Act" << endl;
				ss << lineStr << endl;
				
			} else if( iter.has_tag( m_tag_Action ) )
			{
				ss << "#tag Action" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Character ) )
			{
				ss << "#tag Character" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Parenthetical ) )
			{
				ss << "#tag Parenthetical" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Dialogue ) )
			{
				ss << "#tag Dialogue" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Transition ) )
			{
				ss << "#tag Transition" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Shot ) )
			{
				ss << "#tag Shot" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Notes ) )
			{
				ss << "#tag Notes" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_PlainText ) )
			{
				ss << "#tag PlainText" << endl;
				ss << lineStr << endl;
			} else if( iter.has_tag( m_tag_Scene ) )
			{
				ss << "#tag Scene" << endl;
				ss << lineStr << endl;
			} else
			{
				// Unformatted line
				ss << lineStr << endl;
			}
			
			if( act_num > 0 )
			{
				// store last act
				acts.push_back( ss.str() );
				
				// debug
				// cout << ss.str() << endl;
			}
			break;
		} // if( iter_end.is_end() )
		
		line++;
		iter.set_line( line );
		
	}// while(! iter.is_end() )
	
	// catch single act
	if( act_num > 0 )
	{
		// store last act
		acts.push_back( ss.str() );
		
		// debug
		// cout << ss.str() << endl;
	}
	
	return acts;
}

