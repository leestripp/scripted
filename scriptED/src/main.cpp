#include <string>
#include <locale>

#include <gtkmm/application.h>
#include "scriptED.h"

int main(int argc, char* argv[])
{
	auto app = Gtk::Application::create("com.leestripp.scriptED");
	
	// set the default (global) locale if we want utf-8 for all new streams
	std::locale::global( std::locale( "C.UTF-8" ) );
	
	return app->make_window_and_run<scriptED>( argc, argv, app );
}
