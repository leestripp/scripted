#ifndef MAINMENU_H
#define MAINMENU_H

Glib::ustring mainmenu_info =
  "<interface>"
  "  <menu id='menubar'>"
  "    <submenu>"
  "      <attribute name='label' translatable='yes'>_File</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Open Project</attribute>"
  "          <attribute name='action'>mainmenu.open_project</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;o</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Save</attribute>"
  "          <attribute name='action'>mainmenu.save</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;s</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Export Fountain</attribute>"
  "          <attribute name='action'>mainmenu.export_fountain</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;f</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Export Plain Text</attribute>"
  "          <attribute name='action'>mainmenu.export_plaintext</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;e</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"
  
  "    <submenu>"
  "      <attribute name='label' translatable='yes'>_View</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Zoom In</attribute>"
  "          <attribute name='action'>mainmenu.view_zoomin</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;j</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Zoom Out</attribute>"
  "          <attribute name='action'>mainmenu.view_zoomout</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;k</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"
  
  "    <submenu>"
  "      <attribute name='label' translatable='yes'>_Format</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Scene</attribute>"
  "          <attribute name='action'>mainmenu.format_scene</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;1</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Action</attribute>"
  "          <attribute name='action'>mainmenu.format_action</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;2</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Character</attribute>"
  "          <attribute name='action'>mainmenu.format_character</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;3</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Parenthetical</attribute>"
  "          <attribute name='action'>mainmenu.format_parenthetical</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;4</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Dialogue</attribute>"
  "          <attribute name='action'>mainmenu.format_dialogue</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;5</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Transition</attribute>"
  "          <attribute name='action'>mainmenu.format_transition</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;6</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>_Shot</attribute>"
  "          <attribute name='action'>mainmenu.format_shot</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;7</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Act</attribute>"
  "          <attribute name='action'>mainmenu.format_act</attribute>"
  "          <attribute name='accel'>&lt;Primary&gt;8</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Notes</attribute>"
  "          <attribute name='action'>mainmenu.format_notes</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Plain Text</attribute>"
  "          <attribute name='action'>mainmenu.format_plaintext</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"

  "    <submenu>"
  "      <attribute name='label' translatable='yes'>Ai</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Ingest Current file</attribute>"
  "          <attribute name='action'>mainmenu.lsllamacpp_ingest</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Rewrite Selection</attribute>"
  "          <attribute name='action'>mainmenu.lsllamacpp_rewrite</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Clear Database</attribute>"
  "          <attribute name='action'>mainmenu.lsllamacpp_cleardb</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"

  "    <submenu>"
  "      <attribute name='label' translatable='yes'>Tools</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Split Script by Scene</attribute>"
  "          <attribute name='action'>mainmenu.tools_splitscript</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Split Script by Act</attribute>"
  "          <attribute name='action'>mainmenu.tools_splitacts</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Create Locations</attribute>"
  "          <attribute name='action'>mainmenu.tools_locations</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Create Character List</attribute>"
  "          <attribute name='action'>mainmenu.tools_characters</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"

  "  </menu>"
  "</interface>";

#endif // MAINMENU_H
