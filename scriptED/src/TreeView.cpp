#include "TreeView.h"
#include "TextView.h"

#include <gtkmm/icontheme.h>


TreeView::TreeView()
{
	m_textview = NULL;
	
	/* Create a new scrolled window, with scrollbars only if needed */
	set_policy( Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::AUTOMATIC );

	set_child( m_treeview );
	m_treeview.set_expand();

	//Create the Tree model
	m_refTreeModel = Gtk::TreeStore::create( m_Columns );
	m_treeview.set_model( m_refTreeModel );
	
	//Add the TreeView's column headers.
	m_treeview.append_column( "", m_Columns.m_col_icon );
	m_treeview.append_column( "Item", m_Columns.m_col_name );
	
	// setup icons
	loadIcons();
	
	// **************
	// Popup menu
	m_MenuPopup = new Gtk::PopoverMenu;
	m_MenuPopup->set_parent( *this );
	// Catch right button press events
	auto refGesture = Gtk::GestureClick::create();
	refGesture->set_button( GDK_BUTTON_SECONDARY );
	refGesture->signal_pressed().connect( sigc::mem_fun( *this, &TreeView::on_popup_menu ) );
	add_controller( refGesture );
	
	// Create actions
	auto refActionGroup = Gio::SimpleActionGroup::create();
	refActionGroup->add_action("new_file", sigc::mem_fun(*this, &TreeView::on_popup_new_file));
	refActionGroup->add_action("new_folder", sigc::mem_fun(*this, &TreeView::on_popup_new_folder));
	refActionGroup->add_action("remove", sigc::mem_fun(*this, &TreeView::on_popup_remove));
	refActionGroup->add_action("refresh", sigc::mem_fun(*this, &TreeView::on_popup_refresh));
	// Add group
	insert_action_group( "popup", refActionGroup );
	
	// Dialogs
	m_dialog_file.set_title( "Enter filename" );
	m_dialog_file.set_default_size( 250, 100 );
	m_dialog_file.set_modal();
	m_dialog_file.set_hide_on_close();
	m_dialog_file.buttons_clicked_connect( sigc::mem_fun(*this, &TreeView::on_dialog_file_response) );
	
	m_dialog_dir.set_title( "Enter directory name" );
	m_dialog_dir.set_default_size( 250, 100 );
	m_dialog_dir.set_modal();
	m_dialog_dir.set_hide_on_close();
	m_dialog_dir.buttons_clicked_connect( sigc::mem_fun(*this, &TreeView::on_dialog_dir_response) );
	
	// connect signals
	m_treeview.signal_row_activated().connect( sigc::mem_fun( *this, &TreeView::on_treeview_row_activated ) );
}

TreeView::~TreeView()
{
	if( m_MenuPopup ) delete m_MenuPopup;
}

void TreeView::on_treeview_row_activated( const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column )
{
	// debug
	// cout << "TreeView::on_treeview_row_activated - called" << endl;
	
	const auto iter = m_refTreeModel->get_iter( path );
	if(iter)
	{
		const auto row = *iter;
		
		// debug
		// cout << "Row activated: Type=" << row[m_Columns.m_type] << ", Name=" << row[m_Columns.m_col_name] << endl;
		
		switch( row[m_Columns.m_type] )
		{
			case TVRT_FILE:
			{
				m_textview->loadFile( row[m_Columns.m_full_path] );
				break;
			}
			
			case TVRT_PROJECT:
			case TVRT_DIR:
			{
				if( m_treeview.row_expanded( path ) )
				{
					m_treeview.collapse_row( path );
				} else
				{
					m_treeview.expand_row( path, false );
				}
				break;
			}
			
			default:
				break;
		}
	}
}

// Open Project - build tree view.

void TreeView::openProject( const std::string &path )
{
	// store project path
	m_currentProject = path;
	refreshTree();
}

void TreeView::refreshTree()
{
	// Clear all rows
	m_refTreeModel->clear();
	
	// Add our Root row
	Gtk::TreeModel::Row root = *(m_refTreeModel->append());
	root[m_Columns.m_col_name] = m_currentProject;
	root[m_Columns.m_full_path] = m_currentProject;
	root[m_Columns.m_type] = TVRT_PROJECT;
	loadTree( m_currentProject, root,  0 );
	
	// Get first entry
	Gtk::TreeModel::iterator iter = m_treeview.get_model()->children().begin();
	if( iter )
	{
		// Get path
		Gtk::TreeModel::Path path = m_treeview.get_model()->get_path( iter );
		// Expand the first row.
		m_treeview.expand_row( path, false );
	}
}

void TreeView::loadTree( const std::string &path, Gtk::TreeModel::Row row, int lvl )
{
	if( lvl == 0 )
	{
		// does path exist
		if(! fs::is_directory( path ) )
		{
			return;
		}
	}
	
	// Recursive load oof directory.
	lvl++;
	for( const auto& entry : fs::directory_iterator( path ) )
	{
		const auto dirStr = entry.path().string();
		const auto filenameStr = entry.path().filename().string();
		const auto extStr = entry.path().extension().string();
		
		// debug
		// cout << "lvl: " << lvl << "  path: " << dirStr << "  file: " << filenameStr << endl;
		
		if( entry.is_directory() )
		{
			Gtk::TreeModel::Row new_row = *( m_refTreeModel->append( row.children() ) );
			new_row[m_Columns.m_col_icon] = m_icon_folder;
			new_row[m_Columns.m_col_name] = filenameStr;
			new_row[m_Columns.m_full_path] = dirStr;
			new_row[m_Columns.m_type] = TVRT_DIR;
			// recursive
			loadTree( dirStr, new_row, lvl );
			
		} else
		{
			Gtk::TreeModel::Row new_row = *( m_refTreeModel->append( row.children() ) );
			new_row[m_Columns.m_col_icon] = m_icon_file;
			new_row[m_Columns.m_col_name] = filenameStr;
			new_row[m_Columns.m_full_path] = dirStr;
			new_row[m_Columns.m_type] = TVRT_FILE;
		}
	}
}

void TreeView::loadIcons()
{
	// folder_xpm, file_xpm
	
	m_icon_folder = Gdk::Pixbuf::create_from_xpm_data( folder_16x16_xpm );
	m_icon_file = Gdk::Pixbuf::create_from_xpm_data( file_16x16_xpm );
}

void TreeView::on_popup_menu( int n_press, double x, double y )
{
	const Gdk::Rectangle rect( x, y, 1, 1 );
	m_MenuPopup->set_pointing_to( rect );
	
	auto refSelection = m_treeview.get_selection();
	if( refSelection )
	{
		auto iter = refSelection->get_selected();
		if( iter )
		{
			auto row = *iter;
			
			Glib::ustring name = row[m_Columns.m_col_name];
			// debug
			// cout << "*** Right clicked Item : " << name << endl;
			
			int type = row[m_Columns.m_type];
			// TVRT_PROJECT
			switch( type )
			{
				case TVRT_PROJECT:
				{
					auto gmenu = Gio::Menu::create();
					gmenu->append("_Refresh", "popup.refresh");
					gmenu->append("_New File", "popup.new_file");
					gmenu->append("New Folder", "popup.new_folder");
					m_MenuPopup->set_menu_model( gmenu );
					m_MenuPopup->set_has_arrow( false );
					
					m_MenuPopup->popup();
					break;
				}
				
				case TVRT_DIR:
				{
					auto gmenu = Gio::Menu::create();
					gmenu->append("_New File", "popup.new_file");
					gmenu->append("New Folder", "popup.new_folder");
					gmenu->append("_Remove", "popup.remove");
					m_MenuPopup->set_menu_model( gmenu );
					m_MenuPopup->set_has_arrow( false );
					
					m_MenuPopup->popup();
					break;
				}
				
				case TVRT_FILE:
				{
					auto gmenu = Gio::Menu::create();
					gmenu->append("_Remove", "popup.remove");
					m_MenuPopup->set_menu_model( gmenu );
					m_MenuPopup->set_has_arrow( false );
					
					m_MenuPopup->popup();
					break;
				}
				
				default:
					break;
			}// switch type
		}// if iter
	}// if refSelection
}

void TreeView::on_popup_new_file()
{
	cout << "A popup menu item was selected." << endl;
	
	// Get window
	Gtk::Root *root = get_root();
	Gtk::Window *window = dynamic_cast<Gtk::Window*>(root);
	m_dialog_file.set_transient_for( *window );
	
	// Get filename form user.
	m_dialog_file.set_visible( true );
}

void TreeView::on_popup_new_folder()
{
	cout << "A popup menu item was selected." << endl;
	
	// Get window
	Gtk::Root *root = get_root();
	Gtk::Window *window = dynamic_cast<Gtk::Window*>(root);
	m_dialog_dir.set_transient_for( *window );
	
	// Get dirname form user.
	m_dialog_dir.set_visible( true );
}

void TreeView::on_popup_remove()
{
	cout << "A popup menu item was selected." << endl;
	
	auto refSelection = m_treeview.get_selection();
	if( refSelection )
	{
		auto iter = refSelection->get_selected();
		if( iter )
		{
			auto row = *iter;
			
			string path = row[m_Columns.m_full_path];
			cout << "  Selected Item path : " << path << endl;
			
			int type = row[m_Columns.m_type];
			// TVRT_PROJECT
			switch( type )
			{
				case TVRT_PROJECT:
				{
					// Cant remove a project
					break;
				}
				
				case TVRT_DIR:
				{
					if(! fs::remove_all( path ) )
					{
						cerr << "Could not delete folder : " << path << endl;
					} else
					{
						// delete selected row
						m_refTreeModel->erase( iter ); 
					}
					break;
				}
				
				case TVRT_FILE:
				{
					if(! fs::remove( path ) )
					{
						cerr << "Could not delete : " << path << endl;
					} else
					{
						// delete selected row
						m_refTreeModel->erase( iter ); 
					}
					break;
				}
				
				default:
					break;
			}// switch type
		}// if iter
	}// if refSelection
}

void TreeView::on_popup_refresh()
{
	refreshTree();
}


// dialog responces

void TreeView::on_dialog_file_response( const Glib::ustring& response )
{
	m_dialog_file.set_visible( false );
	
	if( response == "OK" )
	{
		auto refSelection = m_treeview.get_selection();
		if( refSelection )
		{
			auto iter = refSelection->get_selected();
			if( iter )
			{
				auto row = *iter;
				
				string path = row[m_Columns.m_full_path];
				cout << "  Selected Item path : " << path << endl;
				
				if( m_dialog_file.get_entry() != "" )
				{
					string filename = m_dialog_file.get_entry();
					
					ofstream outfile( path + "/" + filename );
					outfile << std::endl;
					outfile.close();
					
					// Add to TreeView
					Gtk::TreeModel::Row new_row = *( m_refTreeModel->append( row.children() ) );
					new_row[m_Columns.m_col_icon] = m_icon_file;
					new_row[m_Columns.m_col_name] = filename;
					new_row[m_Columns.m_full_path] = path + "/" + filename;
					new_row[m_Columns.m_type] = TVRT_FILE;
				} else
				{
					cout << "NOTE: empty filename" << endl;
				}
			}// if iter
		}// refSelection
	}// if( response == "OK" )
}

void TreeView::on_dialog_dir_response( const Glib::ustring& response )
{
	m_dialog_dir.set_visible( false );
	
	if( response == "OK" )
	{
		auto refSelection = m_treeview.get_selection();
		if( refSelection )
		{
			auto iter = refSelection->get_selected();
			if( iter )
			{
				auto row = *iter;
				
				string path = row[m_Columns.m_full_path];
				cout << "  Selected Item path : " << path << endl;
				
				if( m_dialog_dir.get_entry() != "" )
				{
					string dirname = m_dialog_dir.get_entry();
					
					// create folder
					bool val = fs::create_directory( path + "/" + dirname );
					if( val )
					{
						// Add to TreeView
						Gtk::TreeModel::Row new_row = *( m_refTreeModel->append( row.children() ) );
						new_row[m_Columns.m_col_icon] = m_icon_folder;
						new_row[m_Columns.m_col_name] = dirname;
						new_row[m_Columns.m_full_path] = path + "/" + dirname;
						new_row[m_Columns.m_type] = TVRT_DIR;
					}
				} else
				{
					cout << "NOTE: empty directory name" << endl;
				}
			}// if iter
		}// refSelection
	}// if( response == "OK" )
}

